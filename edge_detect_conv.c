
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <time.h>
#include <sys/time.h>
#include <emmintrin.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>    
 
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image/stb_image_write.h"
 
#define FILTER_SIZE 9

#define KERNEL_SIZE 3


int convolve2D(float* in, float* out, int dataSizeX, int dataSizeY,
                float* kernel)
{
    int i, j, m, n;
    float *inPtr, *inPtr2, *outPtr, *kPtr;
    int kCenterX, kCenterY;
    int rowMin, rowMax;                             // to check boundary of input array
    int colMin, colMax;                             //

    // check validity of params
    if(!in || !out || !kernel) return 0;
    if(dataSizeX <= 0 || KERNEL_SIZE <= 0) return 0;

    // find center position of kernel (half of kernel size)
    kCenterX = KERNEL_SIZE >> 1;
    kCenterY = KERNEL_SIZE >> 1;

    // init working  pointers
    inPtr = inPtr2 = &in[dataSizeX * kCenterY + kCenterX];  // note that  it is shifted (kCenterX, kCenterY),
    outPtr = out;
    kPtr = kernel;

    // start convolution
    for(i= 0; i < dataSizeY; ++i)                   // number of rows
    {
        // compute the range of convolution, the current row of kernel should be between these
        rowMax = i + kCenterY;
        rowMin = i - dataSizeY + kCenterY;

        for(j = 0; j < dataSizeX; ++j)              // number of columns
        {
            // compute the range of convolution, the current column of kernel should be between these
            colMax = j + kCenterX;
            colMin = j - dataSizeX + kCenterX;

            *outPtr = 0;                            // set to 0 before accumulate

            // flip the kernel and traverse all the kernel values
            // multiply each kernel value with underlying input data
            for(m = 0; m < KERNEL_SIZE; ++m)        // kernel rows
            {
                // check if the index is out of bound of input array
                if(m <= rowMax && m > rowMin)
                {
                    for(n = 0; n < KERNEL_SIZE; ++n)
                    {
                        // check the boundary of array
                        if(n <= colMax && n > colMin)
                            *outPtr += *(inPtr - n) * *kPtr;
                        ++kPtr;                     // next kernel
                    }
                }
                else
                    kPtr += KERNEL_SIZE;            // out of bound, move to next row of kernel

                inPtr -= dataSizeX;                 // move input data 1 raw up
            }

            kPtr = kernel;                          // reset kernel to (0,0)
            inPtr = ++inPtr2;                       // next input
            ++outPtr;                               // next output
        }
    }

    return 1;
}

int conv2D_intrinsic(float* in, float* out, int data_size_X, int data_size_Y,
                    float* kernel)

{   int NUM_THREADS = 2;
	int kern_cent_X = (KERNEL_SIZE - 1)/2;
	int kern_cent_Y = (KERNEL_SIZE - 1)/2;

	#pragma omp_set_num_threads(NUM_THREADS);  
	float zeropaddedKernel[KERNEL_SIZE*KERNEL_SIZE];
	for(int z = 0; z < KERNEL_SIZE*KERNEL_SIZE; z++)    
		zeropaddedKernel[z] = 0;

	for(int q = -kern_cent_Y; q <= kern_cent_Y; q++) {
		for(int w = -kern_cent_X; w <= kern_cent_X; w++) {
			zeropaddedKernel[(w+kern_cent_X)+(q+kern_cent_Y)*KERNEL_SIZE] = kernel[(kern_cent_X-w)+(kern_cent_Y-q)*KERNEL_SIZE];   
		}
	}

	int newX = data_size_X+((KERNEL_SIZE/2)*2); 
	int newY = data_size_Y+((KERNEL_SIZE/2)*2); 
	newX += (4-(newX%4));
	float zeropadded[newX*newY]; 

	#pragma omp parallel for firstprivate(newX, newY)
	for(int z = 0; z < newX*newY; z++)     
		zeropadded[z] = 0;       

	int a,b;
	#pragma omp parallel for firstprivate(newX,data_size_X, data_size_Y)
	for(b = 0; b < data_size_Y; b++){
		for(a = 0; a < data_size_X; a++){
			zeropadded[(a+(KERNEL_SIZE/2))+(b+(KERNEL_SIZE/2))*newX] = in[a + b*data_size_X];
		}
	}

	#pragma omp parallel 
	{
		__m128 Kernel1;
		__m128 Kernel2;
		__m128 Kernel3;

		__m128 input1 = _mm_setzero_ps();
		__m128 input2 = _mm_setzero_ps();
		__m128 input3 = _mm_setzero_ps();

		__m128 tmp = _mm_setzero_ps();

		int y,x,j,i;
 		#pragma omp for schedule(dynamic, NUM_THREADS) nowait 
		//#pragma omp parallel for firstprivate(tmp, newX, data_size_X, data_size_Y)
		for(y = 0; y < data_size_Y ; y++){
			for(x = 0; x < data_size_X/4*4 ; x+=4){ 
				tmp = _mm_setzero_ps();
				for(j = 0; j < KERNEL_SIZE; j++){ 
					for(i = 0; i < KERNEL_SIZE; i+=3){
						input1 = _mm_loadu_ps(zeropadded + (x+i)+(y+j)*newX);
						input2 = _mm_loadu_ps(zeropadded + (x+i+1)+(y+j)*newX);
						input3 = _mm_loadu_ps(zeropadded + (x+i+2)+(y+j)*newX);

						Kernel1 = _mm_load1_ps(zeropaddedKernel + i+j*KERNEL_SIZE);	
						Kernel2 = _mm_load1_ps(zeropaddedKernel + i+1+j*KERNEL_SIZE);	
						Kernel3 = _mm_load1_ps(zeropaddedKernel + i+2+j*KERNEL_SIZE);	
						tmp = _mm_add_ps(tmp, _mm_mul_ps(input1, Kernel1));
						tmp = _mm_add_ps(tmp, _mm_mul_ps(input2, Kernel2));
						tmp = _mm_add_ps(tmp, _mm_mul_ps(input3, Kernel3));
					}
				}
				_mm_storeu_ps(out + x+y*data_size_X, tmp);
			}
		}

		#pragma omp for schedule(dynamic, NUM_THREADS) nowait 
		for(y = 0; y < data_size_Y ; y++){
			for(x = data_size_X/4*4; x < data_size_X ; x++){ 
				for(j = 0; j < KERNEL_SIZE; j++){ 
					for(i = 0; i < KERNEL_SIZE; i++){
						out[x+y*data_size_X] += zeropaddedKernel[i+j*KERNEL_SIZE] * zeropadded[(x+i) + (y+j)*newX];
					}
				}
			}
		}
	} 

return 1;
}

int main(int argc, char ** argv){
    float *in_image, *out_image;
    int width=300, height=300, channel;
    int size = width * height;

	unsigned char* input_image = stbi_load("einstein.png", &width, &height, &channel, 1);

    in_image = (float*) malloc( width * height * sizeof(float) );
    out_image = (float*) malloc( width * height * sizeof(float) );

    float kernel[] = { -1, -1, -1, 
                   -1, 8, -1, 
                   -1, -1, -1 };

    for (int i = 0; i < size; i++) {
      in_image[i] = ((float) input_image[i])/255;
    
    }

    float start,end,cpu_time_used;

    printf("\nstarting conv slow...\n");
    start = clock();
    convolve2D(in_image, out_image, width, height, kernel);
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("Conv2D Ended in : %lf milliseconds\n\n", cpu_time_used * 1000);


    printf("starting conv fast...\n");
    start = clock();
    conv2D_intrinsic(in_image, out_image, width, height, kernel);
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("Conv2D Intrinsic Ended in : %lf milliseconds\n\n", cpu_time_used * 1000);


    return 0;
}